#!/bin/bash

#本脚本获取系统各项性能参数指标，并与预设阈值进行比较

#time:时间,loalip:eth0网卡IP,free_mem:剩余内存大小,free_disk: 剩余磁盘大小
#cpu_load:15min平均负载,login_user:登录系统的用户,procs:当前进程数量

local_time=$(date +"%Y.%m.%d.%H:%M:%S")
local_ip=$(ifconfig eth0 |grep netmask | tr -s " " |cut -d" " -f3)
free_mem=$(cat /proc/meminfo |grep Avai |tr -s " " |cut -d" " -f2)
free_disk=$(df |grep "/$" |tr -s " " |cut -d " " -f4)
cpu_load=$(cat /proc/loadavg |cut -d " " -f3)
login_user=$(who |wc -l)
procs=$(ps aux | wc -l)

#当剩余内存不足2GB时发送邮件给root进行报警
[ $free_mem -lt 2097152 ] && echo "$local_time Free memory not enough. Free_mem:$free_mem on $local_ip" | mail -s Warning root@zhiyong

#当剩余磁盘不足100GB时发送邮件给root进行报警
[ $free_disk -lt 104857600 ] && echo "$local_time Free disk not enough. root_free_disk:$free_disk on $local_ip" | mail -s Warning root@zhiyong

#当CPU的15min平均负载超过4时发送邮件给root进行报警
result=$(echo "$cpu_load > 4" |bc)
[ $result -eq 1 ] && echo "$local_time CPU load to high,CPU 15 averageload:$cpu_load on $local_ip" | mail -s Warning root@zhiyong

#当系统实时在线人数超过3人时发送邮件给root进行报警
[ $login_user -gt 3 ] && echo "$local_time Too many user. $login_user users login to $local_ip" | mail -s Warning root@zhiyong

#当实时进程数量大于500时发送邮件给root进行报警
[ $procs -gt 500 ] && echo "$local_time Too many procs. $procs proc are running on $local_ip" | mail -s Warning root@zhiyong
