#!/bin/bash

echo -e "\033[34m------服务器硬件信息------\033[0m"

echo -e "\033[32m网卡信息\033[0m"
ifconfig eth0 | grep "inet"

echo -e "\033[32m内存的剩余容量信息：\033[0m"
grep MemAvailable /proc/meminfo

echo -e "\033[32m磁盘根分区的使用情况：\033[0m"
df -h /

echo -e "\033[32m本机CPU型号信息：\033[0m"
grep "model name" /proc/cpuinfo
